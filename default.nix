{ pkgs ? import <nixpkgs> {}, config ? {}, public-url ? "/" }:
let
  nodeDependencies = (
    pkgs.callPackage ./node-dependencies.nix {}
  ).shell.nodeDependencies;
  fetchElmDeps = pkgs.elmPackages.fetchElmDeps {
    elmPackages =
      if builtins.pathExists ./elm-packages.nix
      then
        import ./elm-packages.nix
      else
        {};
    registryDat = ./elm-registry.dat;
    elmVersion = (builtins.fromJSON (
        builtins.readFile ./elm.json
      )).elm-version;
  };
in
pkgs.stdenv.mkDerivation {
  name = "sentry-dom-snapshot-playground";
  src = ./.;
  buildInputs = [
    pkgs.nodejs
    pkgs.utillinux
    pkgs.elmPackages.elm
    pkgs.elm2nix
    pkgs.nodePackages.node2nix
    pkgs.httpie
    pkgs.miniserve
    nodeDependencies
  ];
  buildPhase = "make dist public-url=${public-url}";

  inherit nodeDependencies fetchElmDeps;
}
