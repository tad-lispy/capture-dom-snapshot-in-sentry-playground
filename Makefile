# NOTE: GNU Make is supposed to be run in an environment controlled by Nix.
# If it's not automatically loaded by direnv and lorri, then call:
#
#     make shell
#
# first.

export PATH := $(nodeDependencies)/bin:$(PATH)
export ELM_HOME := $(PWD)/.elm

public-url ?= /

result: clean node-dependencies.nix elm-packages.nix $(shell find src -type f)
	nix build


.PHONY: develop
develop: node-dependencies.nix
	parcel src/index.html

.PHONY: serve
serve: dist
	miniserve --index=index.html dist/

.PHONY: clean
clean:
	rm -rf \
		dist \
		.cache \
		node_modules \
		.elm \
		elm-stuff \
		result \

node-dependencies.nix: package-lock.json
	rm -rf node_modules
	node2nix --development --lock package-lock.json --composition $@
	lorri watch --once

package-lock.json: package.json
	npm install --package-lock-only

elm-packages.nix: elm.json elm-registry.dat
	elm2nix convert > $@

elm-registry.dat:
	# Workaround for https://github.com/cachix/elm2nix/issues/43
	elm2nix snapshot > registry.dat
	mv registry.dat $@


# Call only if not using direnv and Lorri. It will be slow. Use Lorri!
.PHONY: shell
shell:
	nix-shell

# Those targets are run by nix during build

dist: node_modules .elm $(shell find src -type f)
	parcel build --public-url=$(public-url) src/index.html

node_modules:
	ln -s $(nodeDependencies)/lib/node_modules ./node_modules

.elm:
	$(fetchElmDeps)

.PHONY: install
install: dist
	mkdir -p $(out)
	cp -r dist/* $(out)
