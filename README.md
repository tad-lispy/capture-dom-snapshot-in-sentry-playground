# Capture DOM snapshot in Sentry

An experiment to combat Elm run-time errors due to browser extensions mutating DOM that Elm program wants to own.

Here is a simple code that breaks the DOM:

``` coffee
setTimeout (() =>
    document
    .getElementById "counter"
    .remove()),
    500
```

After this, interacting with the program will produce seemingly endless stream of errors.

## Findings

### A lot of errors

Breaking the DOM like this will produce seemingly endless stream of errors. All this errors are sent to Sentry (subject to rate limiting).

To mitigate this I've implemented a client side rate limiting as suggested by `encetamasb` and `lydell` [on Elm Discourse](https://discourse.elm-lang.org/t/breaking-dom-generates-endles-stream-of-runtime-errors/6847). Errors that look alike are reported at most once a minute.

### Breaking the DOM without errors thrown

With some configuration of DOM elements the program do not throw any errors in the production build. For example this:

``` elm
[ Html.div
    [ Html.Attributes.id "counter" ]
    [ model
        |> String.fromInt
        |> Html.text
    ]
, Html.button [ Html.Events.onClick Increment ] [ "+" |> Html.text ]
, Html.button [ Html.Events.onClick Decrement ] [ "-" |> Html.text ]
]
```

Instead, after removal of the `div#counter` containing the counter value, the value is displayed on the first button.

Interestingly, a development build (e.g. using `make develop`) will throw errors.

To consistently get errors, the value is now nested in a `span` element.



