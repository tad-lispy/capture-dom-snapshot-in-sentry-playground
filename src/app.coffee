import { Elm } from "./Main.elm"
import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";



Sentry.init
  dsn: "https://42e4969b17c34e06b566fa8614747d2d@o263473.ingest.sentry.io/5620267"
  integrations: [new Integrations.BrowserTracing()]
  tracesSampleRate: 1.0

# Supress repeating errors reports
sentryReports = {}

Sentry.addGlobalEventProcessor (event) =>

    # If it's not an exception pass it through
    if not event.exception?.values?[0] then return event

    # We consider errors to be the same if they have same type and message ("value" in Sentry nomenclature)
    identifier =
      [ event.exception?.values?[0].type
      , event.exception?.values?[0].value
      ].join ": "

    # If we saw the same error in last 60s - supress it
    lastSeen = sentryReports[identifier] or 0
    if event.timestamp - 60 < lastSeen then return null

    # Otherwise register that we saw it now and proceed
    sentryReports[identifier] = event.timestamp
    return event



# Sent DOM snapshot to Sentry as an attachment
Sentry.addGlobalEventProcessor (event) =>

  # Only send attachemnts for exceptions
  return event unless "exception" of event

  try
    dsn = Sentry
      .getCurrentHub()
      .getClient()
      .getDsn()
    url =
      [ dsn.protocol
      , "://"
      , dsn.host
      , (if dsn.port is "" then ":#{dsn.port}" else "")
      , (if dsn.path is "" then "/#{dsn.path}" else "")
      , "/api/#{dsn.projectId}/events/#{event.event_id}/attachments/?sentry_key=#{dsn.user}&sentry_version=7&sentry_client=custom-javascript"
      ].join ""
    snapshot = document
      .children[0]
      .outerHTML
    content = new Blob [ snapshot ], type: "text/html"

    formData = new FormData();
    formData.append 'dom-snapshot', content, 'dom-snapshot.html'

    console.debug "Sending the snapshot of DOM tree to Sentry"
    fetch url,
      method: 'POST'
      body: formData
    .catch (ex) =>
      # we have to catch this otherwise it throws an infinite loop in Sentry
      console.error(ex);

    return event;

  catch ex
    console.error(ex);

flags = {}

Elm.Main.init { flags }

setTimeout (() =>
    document
    .getElementById "counter"
    .remove()),
    500
