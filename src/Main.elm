module Main exposing (Model, Msg, init, subscriptions, update, view)

import Browser
import Html exposing (Html)
import Html.Attributes
import Html.Events


main : Program Flags Model Msg
main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Flags =
    ()


type alias Model =
    Int


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( 0
    , Cmd.none
    )


type Msg
    = Increment
    | Decrement


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Increment ->
            ( model + 1
            , Cmd.none
            )

        Decrement ->
            ( model - 1
            , Cmd.none
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


view : Model -> Browser.Document Msg
view model =
    Browser.Document
        "Let's capture some errors"
        [ Html.div
            [ Html.Attributes.id "counter" ]
            [ Html.span []
                [ model
                    |> String.fromInt
                    |> Html.text
                ]
            ]
        , Html.button [ Html.Events.onClick Increment ] [ "+" |> Html.text ]
        , Html.button [ Html.Events.onClick Decrement ] [ "-" |> Html.text ]
        ]
